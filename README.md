# renamerlg.py

Cleans the names of PDF, EPUB, and DJVU files. The script processes all files from the current working directory. Runs in Python 3.6+.

```sh
Usage:  renamerlg.py
    -p or --pdf, to process PDF files only
    -d or --djvu, to process DJVU files only
    -e or --epub, to process EPUB files only
    (the default is to process all PDF, DJVU, and EPUB files)
   
    -t or -title-case, to capitalize first word, leaving others unchanged
    -l or -lower-case, to lower the case of all words
    (the default is to use title case according to English rules)
    
    -B or --no-backup, to avoid creating backup
    (the default is to backup)
    
    -v or --verbatim, to show full path of files
    -f <filename> or --file= <filename>, to rename only one file
    -h or --help, to display this message
```

    
# Changes History
- v.1.5 Added other capitalization options
- v.1.4 Refactore to OOP
- v.1.3.1 Check if provided filename includes path
- v.1.3 Added one-file mode
- v.1.2 Refactored code
- v.1.1 Added support for DJVU and EPUB files
- v.1.0 First release

# License
(ɔ) Nicolas Vaughan 2017. Licensed under GPLv3.
