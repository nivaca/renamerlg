#!/usr/bin/env python3

""" renamerlg.py
A Simple PDF file name cleaner v.1.1
© 2017 Nicolas Vaughan
Runs on Python 3+
"""


import os
import sys
import shutil
import re


try:
    from tqdm import tqdm
except ImportError:
    print('Warning: tqdm module not available')
    tqdm_exists = False
else:
    tqdm_exists = True

try:
    from colorama import init, Fore
except ImportError:
    print('Warning: colorama module not available')

    class AnsiCodes(object):
        def __init__(self):
            # the subclasses declare class attributes which are numbers.
            # Upon instantiation we define instance attributes, which are the same
            # as the class attributes but wrapped with the ANSI escape sequence
            for name in dir(self):
                if not name.startswith('_'):
                    value = getattr(self, name)

    class AnsiFore(AnsiCodes):
        BLACK = ''
        RED = ''
        GREEN = ''
        YELLOW = ''
        BLUE = ''
        MAGENTA = ''
        CYAN = ''
        WHITE = ''
        RESET = ''

    Fore = AnsiFore()

    colorama_exists = False
else:
    colorama_exists = True

# -----------------------------------------------------------

default_ext = '.pdf'
# directory = 'mydir/'                      # leave empty for CWD
directory = ''                      # leave empty for CWD
fnames = os.listdir(directory+'.')


replace_list = {'\(b\-ok\.org\)': '',
                '\(bookfi\)': '',
                '\(bookzz\.org\)': '',
                '_+': ' ',
                '-': ' - ',
                '\(': ' (',
                '\)': ') ',
                '[\[\]]': '',
                }


# -----------------------------------------------------------


def islow(mystr):
    prepositions = ['amid', 'anti', 'as', 'at', 'but', 'by', 'down', 'for', 'from', 'in', 'into', 'like', 'near', 'of', 'off', 'on', 'onto', 'over', 'past', 'per', 'plus', 'save', 'than', 'to', 'up', 'upon', 'via', 'with']
    conjunctions = ['and', 'but', 'or', 'nor', 'for', 'yet', 'so']
    articles = ['a', 'an', 'the']
    lowwords = prepositions + conjunctions + articles
    if lowwords.count(mystr) > 0:
        return True
    else:
        return False


def titlecase(inputstr):
    stringlst = inputstr.split(' ')
    for i in range(len(stringlst)):
        lenstr = len(stringlst[i])
        # check if it is the first word of sentence or
        # if it should be capitilized
        if (i == 0) or (not islow(stringlst[i])):
            stringlst[i] = stringlst[i].capitalize()
        # Check whether it begins with '(' and
        # capitalize it always
        try:
            if stringlst[i][0] == '(':
                stringlst[i] = '(' +\
                               stringlst[i][1].capitalize() +\
                               stringlst[i][2:lenstr]
        except:
            pass
    return ' '.join(stringlst)


def get_files():
    file_list = []
    for file in fnames:
        fname = os.path.splitext(file)[0]
        fext = os.path.splitext(file)[1]
        if fext == default_ext:  # check whether file has required extension
            file_list += [fname]
    file_list = sorted(file_list)
    if len(file_list) == 0:
        print(Fore.RED + 'Error: No files to rename')
        exit(0)
    if len(file_list) > 999:
        print(Fore.RED + 'Error: Max number of files to rename exceeds 1000')
        exit(0)
    return file_list


def set_names(file_list, capitalize = True):
    for re_from, re_to in replace_list.items():
        regex = re.compile(re_from, re.IGNORECASE)
        for i in range(len(file_list)):
            file_list[i] = regex.sub(re_to, file_list[i])
    # get rid of multiple spaces
    regex = re.compile(r'\s+')
    for i in range(len(file_list)):
        file_list[i] = regex.sub(' ', file_list[i])
    regex = re.compile(r'^\s+')
    for i in range(len(file_list)):
        file_list[i] = regex.sub('', file_list[i])
    regex = re.compile(r'\s$')
    for i in range(len(file_list)):
        file_list[i] = regex.sub('', file_list[i])
    # capitilize
    if capitalize:
        for i in range(len(file_list)):
            file_list[i] = titlecase(file_list[i])
    # add file extension
    for i in range(len(file_list)):
        file_list[i] += default_ext
    return file_list


def yn_question(question):
    return clean_answer(input(Fore.BLUE + question))


def clean_answer(answer):
    answer = answer.strip()
    answer = answer.lower()
    return answer


def check_answer(answer, bias):
    if (answer=='y') or (answer=='' and bias=='y') or (answer != 'y' and answer != 'n' and bias=='y'):
        return 'y'
    elif (answer=='n') or (answer=='' and bias=='n') or (answer != 'y' and answer != 'n' and bias=='n'):
        return 'n'


def confirmation(orig_flist, dest_flist):
    for input, output in zip(orig_flist, dest_flist):
        print(Fore.RED + input + default_ext, Fore.WHITE  +\
              ' --> \n' + Fore.GREEN + output + '\n')
    answer = yn_question('Rename ' + str(len(orig_flist)) + ' files? [Y/n] ')
    if check_answer(answer, 'y') == 'y':
        return True
    else:
        return False


def create_backup(orig_flist):
    answer = yn_question('Create backup? [N/y] ')
    if check_answer(answer, 'n')=='y':
        bak_dir = directory + 'dir.bak/'
        if not os.path.exists(bak_dir):
            os.makedirs(bak_dir)

            message = ''
            if tqdm_exists:
                bar = tqdm(total=len(orig_flist), desc=message, mininterval=0.1, unit_scale=True)
            else:
                print(message)

            for file in orig_flist:
                file += default_ext
                input_file = directory + file
                output_file = bak_dir + file
                if tqdm_exists:
                   bar.update()
                try:
                    shutil.copyfile(input_file, output_file)
                except OSError as err:
                    print(Fore.RED + 'OS error: {0}'.format(err))
                    exit(0)
        else:
            print(Fore.RED + 'Backup directory already exists! Quitting...')
            exit(0)


def rename_files(orig_flist, dest_flist):

    message = ''
    if tqdm_exists:
        bar = tqdm(total=len(orig_flist), desc=message, mininterval=0.1, unit_scale=True)
    else:
        print(message)

    for in_file, out_file in zip(orig_flist, dest_flist):
        input_file = directory + in_file + default_ext
        output_file = directory + out_file
        if tqdm_exists:
            bar.update()
        try:
            os.rename(input_file, output_file)
        except OSError as err:
            print(Fore.RED + 'OS error: {0}'.format(err))
            exit(0)
    return True




def main():
    """ Main function. """
    capitalize = True
    if len(sys.argv) == 2:
        if (sys.argv[1] == '-n') or (sys.argv[1] == '-n') or (sys.argv[1] == '--no-capitilize'):
            capitalize = False

    if colorama_exists:
        init()

    orig_flist = get_files()
    dest_flist = set_names(get_files(), capitalize)
    if confirmation(orig_flist, dest_flist):
        create_backup(orig_flist)
        if rename_files(orig_flist, dest_flist):
            print(Fore.GREEN + str(len(orig_flist)) + ' file(s) successfully renamed.')
    else:
        print('Cancelled.\n')

if __name__ == '__main__':
    main()
