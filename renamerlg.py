#!/usr/bin/env python3

""" renamerlg.py
A Simple PDF file name cleaner
(ɔ) 2017 Nicolas Vaughan
Runs on Python 3.6+
"""

import os
import sys
import shutil
import re
import getopt
import tempfile

# colorama module --------------------------------
try:
    from colorama import init, Fore
except ImportError:
    print('Warning: colorama module not available')

    class AnsiCodes(object):
        def __init__(self):
            # the subclasses declare class attributes which are numbers.
            # Upon instantiation we define instance attributes, which are the same
            # as the class attributes but wrapped with the ANSI escape sequence
            for name in dir(self):
                if not name.startswith('_'):
                    value = getattr(self, name)

    class AnsiFore(AnsiCodes):
        BLACK = ''
        RED = ''
        GREEN = ''
        YELLOW = ''
        BLUE = ''
        MAGENTA = ''
        CYAN = ''
        WHITE = ''
        RESET = ''

    Fore = AnsiFore()

    colorama_exists = False
else:
    colorama_exists = True


# -----------------------------------------------------------
# GLOBALS

version = '1.5'
onefilemode = False
do_backup = True

english_title_case = True
title_case = False
lower_case = False

verbatim = False

extensions = ['.pdf',
              '.djvu',
              '.epub']

# directory = 'tmp/'                      # leave empty for CWD
directory = ''                      # leave empty for CWD


# -----------------------------------------------------------


class File:
    """ This is the class of files."""

    def __init__(self, name):
        self.temp_name = name  # without cleaning

        self.name = ""  # only name
        self.extension = ""  # only extension
        self.name_ext = ""  # file and extension
        self.full_name = ""  # includes full path

        self.destination_name = ""  # only name
        self.destination_extension = ""  # only extension
        self.destination_name_ext = ""  # file and extension
        self.destination_full_name = ""  # includes full path

        self.path = ""  # will hold full path (without the file name)

        self.get_path()
        self.set_names()
        self.exists = self.pexists()

    def get_path(self):
        path = os.path.dirname(self.temp_name)
        if path == '':
            self.path = os.getcwd() + '/'
        else:
            self.path = path + '/'

    def set_names(self):
        fullname = os.path.basename(self.temp_name)
        self.name = os.path.splitext(fullname)[0]
        self.extension = os.path.splitext(fullname)[1]
        self.name_ext = self.name + self.extension
        self.full_name = self.path + self.name_ext

    def pexists(self):
        # if not onefilemode, the existence is a given
        if not onefilemode:
            return True
        elif os.path.exists(self.name):
            return True
        else:
            return False

    def prepare_rename(self):
        dest_name = self.name

        regex = re.compile(r'kupdf.net_', re.IGNORECASE)
        dest_name = regex.sub('', dest_name)

        regex = re.compile(r'kupdf.com_', re.IGNORECASE)
        dest_name = regex.sub('', dest_name)

        regex = re.compile(r'\(b-ok.org\)', re.IGNORECASE)
        dest_name = regex.sub('', dest_name)

        regex = re.compile(r'\(bookfi\)', re.IGNORECASE)
        dest_name = regex.sub('', dest_name)

        regex = re.compile(r'\(bookzz.org\)', re.IGNORECASE)
        dest_name = regex.sub('', dest_name)

        regex = re.compile(r'\(', re.IGNORECASE)
        dest_name = regex.sub(' (', dest_name)

        regex = re.compile(r'\)', re.IGNORECASE)
        dest_name = regex.sub(') ', dest_name)

        regex = re.compile(r'\[', re.IGNORECASE)
        dest_name = regex.sub('', dest_name)

        regex = re.compile(r'\]', re.IGNORECASE)
        dest_name = regex.sub('', dest_name)

        # regex = re.compile(r'-')
        # dest_name = regex.sub(' ', dest_name)

        # '_' => ' '
        regex = re.compile(r'_')
        dest_name = regex.sub(' ', dest_name)

        # get rid of multiple spaces
        regex = re.compile(r'\s+')
        dest_name = regex.sub(' ', dest_name)
        #
        regex = re.compile(r'^\s+')
        dest_name = regex.sub('', dest_name)
        #
        regex = re.compile(r'\s$')
        dest_name = regex.sub('', dest_name)

        # lowercase extension --- always!!!
        dest_ext = self.extension.lower()

        # CAPITALIZATION
        # capitalize file name according to English rules
        if english_title_case:
            dest_name = titlecase(dest_name)
        # only capitalizes the first word of the title, leaving other words unchanged
        elif title_case:
            dest_name = dest_name[0].upper() + dest_name[1:]
        # lower case everything
        elif lower_case:
            dest_name = dest_name.lower()

        self.destination_name = dest_name
        self.destination_extension = dest_ext
        self.destination_name_ext = dest_name + dest_ext
        self.destination_full_name = self.path + self.destination_name_ext

    def needed(self):
        """Check if rename is really needed."""
        if self.name != self.destination_name:
            return True
        elif self.extension != self.destination_extension:
            return True
        else:
            return False

    def rename_me(self):
        """Rename the file."""
        try:
            os.rename(self.full_name, self.destination_full_name)
        except OSError as err:
            print(Fore.RED + 'OS error: {0}'.format(err))
            sys.exit(1)


# ==============================================================


def islow(mystr):
    prepositions = ['amid', 'anti', 'as', 'at', 'but', 'by', 'down',
                    'for', 'from', 'in', 'into', 'like', 'near', 'of',
                    'off', 'on', 'onto', 'over', 'past', 'per', 'plus',
                    'save', 'than', 'to', 'up', 'upon', 'via', 'with']
    conjunctions = ['and', 'but', 'or', 'nor', 'for', 'yet', 'so']
    articles = ['a', 'an', 'the']
    lowwords = prepositions + conjunctions + articles
    if lowwords.count(mystr) > 0:
        return True
    else:
        return False


# ==============================================================


def titlecase(inputstr):
    stringlst = inputstr.split(' ')
    for i in range(len(stringlst)):
        lenstr = len(stringlst[i])
        # check if it is the first word of sentence or
        # if it should be capitilized
        if (i == 0) or (not islow(stringlst[i])):
            stringlst[i] = stringlst[i].capitalize()
        # Check whether it begins with '(' and
        # capitalize it always
        try:
            if stringlst[i][0] == '(':
                stringlst[i] = '(' + \
                               stringlst[i][1].capitalize() + \
                               stringlst[i][2:lenstr]
        except IndexError:
            pass
    return ' '.join(stringlst)


# ==============================================================


def get_files():
    files = []
    allfilenames = os.listdir(directory + '.')
    for fi in allfilenames:
        ext = os.path.splitext(fi)[1]
        # check if file of proper extension
        if ext.lower() in extensions:
            files.append(File(fi))
    # returns a lists of file objects
    return files


# ==============================================================

def yn_question(question):
    return clean_answer(input(Fore.BLUE + question))


def clean_answer(answer):
    answer = answer.strip()
    answer = answer.lower()
    return answer


def check_answer(answer, bias):
    if (answer == 'y') or (answer == '' and bias == 'y') or (answer != 'y' and answer != 'n' and bias == 'y'):
        return 'y'
    elif (answer == 'n') or (answer == '' and bias == 'n') or (answer != 'y' and answer != 'n' and bias == 'n'):
        return 'n'


def confirmation(files):
    for f in files:
        if verbatim:
            print(Fore.RED + f.full_name)
            print(Fore.WHITE + ' -> ' + Fore.GREEN + f.destination_full_name)
        else:
            print(Fore.RED + f.name_ext, Fore.WHITE + ' -> ' +
                  Fore.GREEN + f.destination_name_ext, Fore.WHITE)

    if not verbatim:
        print('')

    answer = yn_question('Rename ' + str(len(files)) + ' file(s)? [Y/n] ')
    if check_answer(answer, 'y') == 'y':
        return True
    else:
        return False


def backup(files):
    if do_backup:
        tempdir = tempfile.mkdtemp()
        print(f'Backup directory: {tempdir}')
        for f in files:
            try:
                shutil.copyfile(f.full_name, tempdir + '/' + f.name_ext)
            except OSError as err:
                print(Fore.RED + 'OS error: {0}'.format(err))
                sys.exit(1)


def rename(files):
    backup(files)
    for f in files:
        f.rename_me()


def usage():
    print(f"renamerlg.py v.{version}. (ɔ) Nicolas Vaughan 2017.", """
Usage: renamerlg.py
    -p or --pdf, to process PDF files only
    -d or --djvu, to process DJVU files only
    -e or --epub, to process EPUB files only
    (the default is to process all PDF, DJVU, and EPUB files)

    -t or -title-case, to capitalize first word, leaving others unchanged
    -l or -lower-case, to lower the case of all words
    (the default is to use title case according to English rules)

    -B or --no-backup, to avoid creating backup
    (the default is to backup)

    -v or --verbatim, to show full path of files
    -f <filename> or --file= <filename>, to rename only one file
    -h or --help, to display this message
    """)


# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
def main():
    """ Main function. """
    global extensions
    global onefilemode
    global directory
    global do_backup
    global english_title_case
    global title_case
    global lower_case
    global verbatim

    files = []  # list of file objects

    if colorama_exists:
        init()

    letter_options = "pde" + "B" + "tl" + "hvf:"
    letter_options_list = []
    for i in range(0, len(letter_options)):
        if letter_options[i] == ':':
            pass
        else:
            letter_options_list.append('-' + letter_options[i])

    word_options = ["pdf", "djvu", "epub",
                    "no-backup",
                    "title-case",  # capitalise the first word leaving others unchanged
                    "lower-case",
                    "help", "verbatim", "file="]

    try:
        opts, args = getopt.getopt(sys.argv[1:], letter_options, word_options)
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-p", "--pdf"):
            extensions = ['.pdf']
        if opt in ("-d", "--djvu"):
            extensions = ['.djvu']
        if opt in ("-e", "--epub"):
            extensions = ['.epub']

        if opt in ("-B", "--no-backup"):
            do_backup = False

        if opt in ("-t", "--title-case"):
            english_title_case = False
            title_case = True

        if opt in ("-l", "--lower-case"):
            english_title_case = False
            lower_case = True

        if opt in ("-h", "--help"):
            usage()
            sys.exit(2)

        if opt in ("-f", "--file"):
            onefilemode = True
            onefilename = arg

        if opt in ("-v", "--verbatim"):
            verbatim = True

        if opt not in (letter_options_list + word_options):
            usage()
            sys.exit(2)

    if onefilemode:
        files.append(File(onefilename))
        if not files[0].exists:
            print(Fore.RED + f'File {onefilename} does not exist!\nAborting.')
            sys.exit(1)
    # get all files from the CWD
    else:
        # populate file list with File objects
        files = get_files()

    for f in files:
        f.prepare_rename()

    # expunges list from unneeded renamings
    # https://stackoverflow.com/a/1208792/1815288
    files[:] = (f for f in files if f.needed())

    if len(files) == 0:
        print(Fore.RED + 'No files to rename.')
        sys.exit(0)

    if confirmation(files):
        rename(files)


if __name__ == '__main__':
    main()
